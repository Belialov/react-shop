import React, {Component} from 'react';
import NavBar from './components/NavBar';
import Products from './components/Products'
import Cart from './components/Cart'

class App extends Component {
  removeFromCart = (obj) => {
    let newTotalPrice = this.state.totalPrice;
    newTotalPrice -= obj.price*obj.q;
    const newCart = [...this.state.cart]
    for (const index in newCart) {
      if (newCart[index].id === obj.id) {
        newCart[index].q = 1;
        newCart.splice(index,1)
      }
    }


    this.setState({cart: newCart, totalPrice: newTotalPrice});
  }

  addToCart = (obj) => {
    let newTotalPrice = this.state.totalPrice;
    newTotalPrice += obj.price;
    const  newCart = [...this.state.cart];
    let trigger = false;
    for (const item of newCart) {
      if (item.id === obj.id) {
        ++item.q;
       trigger = true;
      }
    }
    !trigger && newCart.push(obj)
    this.setState({cart: newCart, totalPrice: newTotalPrice});
  };

  updateQ = (value,obj) => {
    let newTotalPrice = this.state.totalPrice;
    
    const  newCart = [...this.state.cart];
    for (let index in newCart) {
      if (newCart[index].id === obj.id) {
       newCart[index].q > value && (newTotalPrice -= obj.price);
       newCart[index].q < value && (newTotalPrice += obj.price);
        if (value >= 1) {
          newCart[index].q=value;
         
        } else {
          newCart.splice(index,1);
        }
    
      }
    }


    this.setState({cart: newCart, totalPrice: newTotalPrice});
  }

  state = {
    totalPrice:0,
    cart: [],
    cars: [
      {
        model: 'IS250',
        price: 100,
        img: 'images/is.png',
        id: 1,
        q: 1
      }, {
        model: 'ES350',
        price: 200,
        img: 'images/es_s.png',
        id: 2,
        q: 1
      }, {
        model: 'GS350 F-SPORT',
        price: 300,
        img: 'images/gsf_s.png',
        id: 3,
        q: 0
      }, {
        model: 'GS350',
        price: 400,
        img: 'images/gs_s.png',
        id: 4,
        q: 1
      }, {
        model: 'LC500',
        price: 500,
        img: 'images/lc_s.png',
        id: 5,
        q: 1
      }, {
        model: 'LS500',
        price: 400,
        img: 'images/ls_s.png',
        id: 6,
        q: 1
      }, {
        model: 'LX570',
        price: 400,
        img: 'images/lx_s.png',
        id: 7,
        q: 0
      }, {
        model: 'NX250',
        price: 400,
        img: 'images/nx_s.png',
        id: 8,
        q: 1
      }, {
        model: 'RC-F',
        price: 400,
        img: 'images/rcf_s.png',
        id: 9,
        q: 1
      }, {
        model: 'RX350-XL',
        price: 400,
        img: 'images/rxl_s.png',
        id: 10,
        q: 1
      }, {
        model: 'GX460',
        price: 400,
        img: 'images/gx_s.png',
        id: 11,
        q: 1
      }
    ]
  }
  render() {
    return (
      <div><NavBar cartIcon={this.state.cart.length} />
        <div className='container'><Products addToCart={this.addToCart} cars={this.state.cars}/></div>
        <div className='container'><Cart totalPrice={this.state.totalPrice} updateQ={this.updateQ} removeFromCart={this.removeFromCart} cart={this.state.cart}/></div>
      </div>
    );
  }
}

export default App;
