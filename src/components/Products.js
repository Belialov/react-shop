import React, {Component} from 'react';

class Products extends Component {
  render() {
    return (
      <ul className='items'>
        {this
          .props
          .cars
          .map(obj => (
            <li key={obj.id} className='item'>
              <div className='img'><img src={obj.img} alt="car pic"/></div>
              <h4>{obj.model}</h4>
              <h6>Starting from: {obj.price}$
                <button onClick={()=> this.props.addToCart(obj)} className='btn btn-success btn-sm'>BUY</button>
              </h6>
            </li>
          ))}
      </ul>
    );
  }
}

export default Products;