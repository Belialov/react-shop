import React, {Component} from 'react';

class Cart extends Component {




  render() {
  
    return (
      <div>
        {this.props.cart.length === 0 && (
          <h2>Your cart is empty</h2>
        )}
        <ul className="items">
          {this
            .props
            .cart
            .map((obj,index) => (
              <li key={index}>
                  <div className="img">
                  <img src={obj.img} alt="car img"/>
                  </div>
                  <h4>{obj.model}</h4>
                  <h6>UNIT COST: {obj.price}</h6>
                  <span>Quantity: 
                    <input type="number" id="quant" value={obj.q} onChange={(event) => {this.props.updateQ(event.target.value, obj)}} />
                  </span>
                  <button onClick={() => this.props.removeFromCart(obj)} className='btn btn-warning btn-sm'>REMOVE</button>
              </li>
            ))}
        </ul>
        {this.props.totalPrice > 0 && (<h3>Total price: {this.props.totalPrice} $</h3>)}
      </div>
    );
  }
}

export default Cart;